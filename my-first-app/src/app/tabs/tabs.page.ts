import { Component } from '@angular/core';

import { IonRouterOutlet, Platform, ToastController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
const { App } = Plugins;
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;


  constructor(private platform: Platform, private routerOutlet: IonRouterOutlet, public toastCtrl: ToastController) {

    this.platform.backButton.subscribeWithPriority(-1, () => {
    //Double check to exit app
    if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
        if (!this.routerOutlet.canGoBack()) {
          App.exitApp();
        }
    } else {
      const toast = this.toastCtrl.create({
        message: 'Press back again to exit App',
        duration: 3000,
        position: 'bottom'
      }).then(toast => toast.present());;

      this.lastTimeBackPress = new Date().getTime();
    }
  });


  }
}
