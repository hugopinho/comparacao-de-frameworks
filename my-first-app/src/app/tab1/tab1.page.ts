import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  data: any;
  constructor() {}


  ngOnInit() {
    fetch('http://www.json-generator.com/api/json/get/cgfhCVDgUi?indent=2').then(res => res.json())
    .then(json => {
      this.data = json;
    });
  }
}
