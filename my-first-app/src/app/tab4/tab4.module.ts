import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Tab4Page } from './tab4.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';


import { Tab4PageRoutingModule } from './tab4-routing.module';

import { BadgeModule } from "@nossis-ui-ngx/components/presentation";
import {TagModule} from "@nossis-ui-ngx/components/presentation";

@NgModule({
  imports: [
    IonicModule,
    BadgeModule,
    TagModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: Tab4Page }]),
    Tab4PageRoutingModule
  ],
  declarations: [Tab4Page]
})
export class Tab4PageModule {}
