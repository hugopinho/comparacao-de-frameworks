import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})
export class Tab4Page implements OnInit{

  registrationForm: FormGroup;
  dynamicFormArray: any;



  constructor(private httpClient: HttpClient, private fb:FormBuilder) {

  }


  ngOnInit(): void {
    let control = new FormControl('');
    this.registrationForm = this.fb.group({

     // 'email': ['', Validators.compose([Validators.required, Validators.minLength(3)])],
    });


    this.httpClient.get('http://www.json-generator.com/api/json/get/cfTbTDIIAy?indent=2').subscribe(data => {
      this.dynamicFormArray = data;

      //we call method here so once responde binded to dynamicformArray
      this.createFormControl();
    })

  }


  createFormControl(){
    //in this method we create formControl based on dynamiFormArray

    this.dynamicFormArray.forEach(element => {

      if(element.validation.minLen && element.validation.required){
        this.registrationForm.addControl(element.id, new FormControl('',
        [
          Validators.required,
          Validators.minLength(element.validation.minLen)
        ]));
        console.log(element.validation.minLen);
      }else{
        if(element.validation.required === true){
          this.registrationForm.addControl(element.id, new FormControl('',Validators.required));

        }else{
          this.registrationForm.addControl(element.id, new FormControl(''))
        }
      }



    });
  }

  onSubmit(): void {
    console.log(this.registrationForm.value);
    this.registrationForm.reset()
  }



}
