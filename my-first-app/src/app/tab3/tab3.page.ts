import { Component, OnInit, OnDestroy } from '@angular/core';
import * as L from 'leaflet';
import { antPath } from 'leaflet-ant-path';

import 'leaflet/dist/images/marker-icon-2x.png'
import 'leaflet/dist/images/marker-shadow.png'
import 'leaflet.BounceMarker'


import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  imageSrc = 'assets/markers/pin.png'
  mapSrc = 'assets/Portugal'
  map: L.Map;
  myMarker;
  localizacaoAtual;
  lat;
  lng;
  watch:any;
  subscription: any;

  customIcon = L.icon({
    iconUrl:this.imageSrc,
    iconSize:     [25, 25], // size of the icon
    shadowSize:   [25, 25], // size of the shadow
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [0, -5] // point from which the popup should open relative to the iconAnchor
});

 constructor(private geolocation: Geolocation) { }


  ngOnInit() {

  //obtem localicação atual do device e inicia mapa
    // resp.coords.latitude -  resp.coords.longitude
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('this map', this.map);

        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;
        this.map.setView(new L.LatLng( this.lat, this.lng),15);

        this.localizacaoAtual = L.marker({lat: this.lat, lng:  this.lng},{icon: this.customIcon,bounceOnAdd:true }, );
        this.localizacaoAtual.bindPopup('Localização atual do dispostivo',{
          closeButton: true
        }).addTo(this.map)


     }).catch((error) => {
       console.log('Error getting location', error);
     });



     this.map = L.map('mapId',{
      center: [ 40.159711275630556, -8.498394190985822 ],
      zoom: 7,
      renderer: L.canvas()
    })


    var basemaps = {
      Topography: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }),

      Places: L.tileLayer.wms('http://ows.mundialis.de/services/service?', {
          layers: 'OSM-Overlay-WMS'
      }),

      'Topography, then places': L.tileLayer.wms('http://ows.mundialis.de/services/service?', {
          layers: 'TOPO-WMS,OSM-Overlay-WMS'
      }),

      'Places, then topography': L.tileLayer.wms('http://ows.mundialis.de/services/service?', {
          layers: 'OSM-Overlay-WMS,TOPO-WMS'
      })
  };

    /* Mapa offline
    L.tileLayer(this.mapSrc+'/{z}/{x}/{y}.png', {
      attribution: 'Hugo Pinho © Angular LeafLet',
    }).addTo(this.map)
    */

   /* L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© OpenStreetMap contributors',
    }).addTo(this.map)*/



    L.control.layers(basemaps).addTo(this.map);

    basemaps.Topography.addTo(this.map);

    this.addMarker()

    setTimeout(() => {
      this.map.invalidateSize();
    }, 0);


  }


  ionViewWillEnter() {
    setTimeout(() => {
      this.map.invalidateSize();
    }, 0);
  }



  addMarker() {

    /*
    Watch alteração de coordenadas do device
     let watch = this.geolocation.watchPosition();
     this.subscription =  watch.subscribe((data) => {
      if ("coords" in data) {

        if(this.myMarker != null){
          this.map.removeLayer(this.myMarker);
          this.myMarker = null;
        }else{
          this.myMarker = L.marker({lat: data.coords.latitude, lng: data.coords.longitude}, {draggable:true, bounceOnAdd:true});
          this.myMarker.bindPopup('This is out Home Marker',{
            closeButton: true
          }).addTo(this.map)
        }
      } else {
        console.log("Não existe coords dentro de data");
      }
      });
    */

   /* Cirulo

    L.circle({lat:40.15131445860255, lng:  -8.501140772972962}, {
        color:'steelblue',
        radius:500,
        fillColor:'steelblue',
        opcaity:0.5
      }).addTo(this.map)
    */

      // Click listener no mapa
    this.map.on("click", e => {
      if (this.myMarker) { // check
        this.map.removeLayer(this.myMarker); // remove
        this.myMarker = null;
        console.log(this.myMarker); // get the coordinates
      }else{
        console.log(e.latlng);
        //this.myMarker = new L.marker([e.latlng.lat, e.latlng.lng]);
        this.myMarker = L.marker({lat:e.latlng.lat, lng:  e.latlng.lng}, {draggable:true, bounceOnAdd:true});
        this.myMarker.bindPopup('Lat: ' + e.latlng.lat + " long: " + e.latlng.lng,{
          closeButton: true
        }).addTo(this.map)
      }
    });


  }
/* para remover o watch senao fica a consumir recursos
  ionViewDidLeave() {
    this.subscription.unsubscribe();
  }*/

}
