import React from 'react';
import {StyleSheet, Image, Text, TouchableOpacity} from 'react-native';


export default class Card extends React.PureComponent {


    render() {
        return (
            <TouchableOpacity style={styles.card} onPress={ this.props.funcao}>
                <Image style={styles.cardImage} source={{uri:this.props.item.picture}}/>
                <Text style={styles.title}> {this.props.item.name} </Text>
                <Text h4 style={styles.cardText}> {this.props.item.email} </Text>
            </TouchableOpacity>
        )
    }
}



const styles = StyleSheet.create({
    card: {
        backgroundColor:'#fff',
        marginBottom:10,
        marginLeft: '2%',
        width:'96%',
        shadowColor:"#000",
        shadowOpacity:0.2,
        shadowRadius:1,
        shadowOffset:{
            width:3,
            height:3
        }
    },
    cardImage:{
        width:'100%',
        height:200,
        resizeMode:'cover'
    },
    cardText:{
        padding:10,
     },
     title:{
        padding:10,
        fontSize:16
    }
  });