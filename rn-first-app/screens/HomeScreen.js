import React from 'react';
import {StyleSheet, View, FlatList, ActivityIndicator, Button} from 'react-native';
import Header from '../components/Header';
import Card from '../components/Card';

export default class HomeScreen extends React.PureComponent {
 
   constructor() {
    super();
    this.state = {
      items: []
    }
  }

  componentDidMount() {
    this._get('http://www.json-generator.com/api/json/get/cgfhCVDgUi?indent=2').then(
      data => {
        this.setState({items:data})
      }   
    )
  }
  
     // get(endpoint)
    _get = async (endpoint) => {
    const res = await fetch(endpoint);
    const data = await res.json();
    return data;
    }

    navigateToMap = () => { 
    this.props.navigation.navigate('Map');
    }

    navigateToMapBox = () => { 
      this.props.navigation.navigate('Map');
      }

    render() {
        if(this.state.items.length ===0){
            return(
              <View style={styles.loader}>
                <ActivityIndicator size="large"/>
              </View>
            );
          }else{
            return(
                <View style={styles.screen}>
                 
                <View style={styles.container}>
                    <FlatList
                    data={this.state.items} 
                    keyExtractor={ (item,index) => index.toString()} 
                    renderItem={
                        ({item}) => 
                        <Card item={item} funcao={this.navigateToMap} />
                    }
                    />
                </View>
                
                </View>
            )
        }   
    }
}


const styles = StyleSheet.create({
    screen:{
        flex:1
      },
    container: {
       
        backgroundColor: '#5c5c5c',
      },
  loader: {
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  }
  });