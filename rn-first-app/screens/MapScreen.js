import React from "react";
import { StyleSheet, View, Dimensions, Text } from "react-native";
import MapView from "react-native-maps";
import { PROVIDER_GOOGLE } from 'expo';
import {
  UrlTile,
  Marker,
  Geojson,
  ProviderPropType,
  WMSTile,
  MAP_TYPES,
  Callout,
} from "react-native-maps";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 39.694445291005756;
const LONGITUDE = -8.13021077352532;
const LATITUDE_DELTA = 10;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const OVERLAY_TOP_LEFT_COORDINATE2 = [39.694445291005756, -8.13021077352532];
const OVERLAY_BOTTOM_RIGHT_COORDINATE2 = [
  38.694445291005756,
  -7.13021077352532,
];
const IMAGE_URL2 = "https://maps.gsi.go.jp/xyz/std/17/116423/51615.png";

const myPlace = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      properties: {},
      geometry: {
        type: "Point",
        coordinates: [39.694445291005756, -8.13021077352532],
      },
    },
  ],
};

/**   <MapView style={styles.map} /> */
export default class MapScreen extends React.PureComponent {
 

  constructor(props) {

    super(props);

    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      overlay2: {
        bounds: [
          OVERLAY_TOP_LEFT_COORDINATE2,
          OVERLAY_BOTTOM_RIGHT_COORDINATE2,
        ],
        image: IMAGE_URL2,
      },
      poi: null,
      markers: [] ,
      marker: null
    };

    this.onPoiClick = this.onPoiClick.bind(this);

  }

  onPoiClick(e) {
    this.setState({ marker: e.nativeEvent.coordinate })
  // this.setState({ markers: [...this.state.markers, { latlng: e.nativeEvent.coordinate }] })
  }


  render() {
    const { region } = this.state;
    return (
      <View style={styles.container}
     >
        
        <MapView
          provider={PROVIDER_GOOGLE}
          mapType={MAP_TYPES.STANDARD}
          style={styles.map}
          initialRegion={region}
          onPress={this.onPoiClick }>
          
          <MapView.Overlay
            bounds={this.state.overlay2.bounds}
            image={this.state.overlay2.image}
            opacity={0.1}
          />

        {
          this.state.marker &&
          <MapView.Marker coordinate={this.state.marker} >
             <Callout>
                <View>
                  <Text>Selecionou</Text>
                  <Text>{this.state.marker.latitude.toString()  + " \n" + this.state.marker.longitude.toString() } </Text>
                </View>
              </Callout>
          </MapView.Marker>
        }
        
        <Geojson geojson={myPlace} />

        </MapView>
      
      </View>
    );
  }
}

MapScreen.propTypes = {
  provider: ProviderPropType,
};

/*
 mapa por cima de google maps
    <UrlTile
            urlTemplate="http://c.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg"
            



            zIndex={-1}
          />
*/

/* ARRAY
     {
              this.state.markers.map((marker, i) => (
                  <MapView.Marker key={i} coordinate={marker.latlng} >
                    
                  </MapView.Marker>
              ))
          }
*/
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  bubble: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

/*
         <MapView.Overlay
            bounds={this.state.overlay2.bounds}
            image={this.state.overlay2.image}
            opacity={0.3}
          />

        {
          this.state.marker &&
          <MapView.Marker coordinate={this.state.marker} >
             <Callout>
                <View>
                  <Text>Selecionou</Text>
                  <Text>{this.state.marker.latitude.toString()  + " \n" + this.state.marker.longitude.toString() } </Text>
                </View>
              </Callout>
          </MapView.Marker>
        }
        
        <Geojson geojson={myPlace} />
 
   */
