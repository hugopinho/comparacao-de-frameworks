import 'dart:collection';

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import "package:latlong/latlong.dart" as latLng;
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Users'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<List<User>> _getUsers() async {
    var data = await http
        .get("http://www.json-generator.com/api/json/get/cgfhCVDgUi?indent=2");

    var jsonData = json.decode(data.body);

    List<User> users = [];

    for (var u in jsonData) {
      User user =
          User(u["index"], u["about"], u["name"], u["email"], u["picture"]);

      users.add(user);
    }

    print(users.length);

    return users;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: Container(
        child: FutureBuilder(
          future: _getUsers(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print(snapshot.data);
            if (snapshot.data == null) {
              return Container(child: Center(child: Text("Loading...")));
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: CircleAvatar(
                      backgroundImage:
                          NetworkImage(snapshot.data[index].picture),
                    ),
                    title: Text(snapshot.data[index].name),
                    subtitle: Text(snapshot.data[index].email),
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => DetailPage()));
                      // DetailPage(snapshot.data[index])));
                    },
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}

class DetailPage extends StatefulWidget {
  //DetailPage(this.title);
  DetailPage();
  //final String title;

  @override
  _DetailPageState createState() => new _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  // User user;
  MapController _mapController = MapController();

  List<Marker> _markers = List<Marker>();
  //var _markers = [];
  var points = <latLng.LatLng>[
    latLng.LatLng(40.71, -74.00),
    latLng.LatLng(42.71, -73.00),
    latLng.LatLng(45.71, -76.00),
  ];

  @override
  void initState() {
    super.initState();

    setState(() {
      _markers.add(new Marker(
          width: 45.0,
          height: 45.0,
          point: new latLng.LatLng(40.71, -74.00),
          builder: (cointainer) => new Container(
                child: IconButton(
                  icon: Icon(Icons.location_on),
                  color: Colors.red,
                  iconSize: 45.0,
                  onPressed: () {
                    print('marker pressionado');
                  },
                ),
              )));
    });
  }

  // DetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MAPA"),
        //title: Text(user.name),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.zoom_out),
        onPressed: () {
          var newZoom = _mapController.zoom - 1;
          _mapController.move(_mapController.center, newZoom);
        },
      ),
      body: new FlutterMap(
        mapController: _mapController,
        options: MapOptions(
          center: new latLng.LatLng(40.71, -74.00),
          zoom: 10.0,
          //onTap: (point) => print(point),
          onTap: (point) => addMarkert(point),
        ),
        layers: [
          TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c']),
          new PolylineLayerOptions(polylines: [
            new Polyline(
              points: points,
              color: Colors.green,
              strokeWidth: 4.0,
            ),
          ]),
          new MarkerLayerOptions(
            markers: _markers,
          ),
        ],
      ),
    );
  }

  newMethod() {
    FlutterMap(
      options: MapOptions(
        // center: latLng.LatLng(51.5, -0.09),
        zoom: 13.0,
      ),
      layers: [
        TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c']),
      ],
    );
  }

  addMarkert(var point) {
    print(_markers.length);
    print(point);
    setState(() {
      _markers.add(new Marker(
          width: 45.0,
          height: 45.0,
          point: point,
          builder: (cointainer) => new Container(
                child: IconButton(
                  icon: Icon(Icons.location_on),
                  color: Colors.blue,
                  iconSize: 45.0,
                  onPressed: () {
                    Fluttertoast.showToast(
                        msg: "Tem as seguintes coordenadas $point",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0);
                  },
                ),
              )));
    });
    print(_markers[_markers.length - 1]);
  }

  /* setState(() {
      _markers.add(new Marker(
          width: 45.0,
          height: 45.0,
          point: new latLng.LatLng(40.71, -74.00),
          builder: (cointainer) => new Container(
                child: IconButton(
                  icon: Icon(Icons.location_on),
                  color: Colors.red,
                  iconSize: 45.0,
                  onPressed: () {
                    print('marker pressionado');
                  },
                ),
              )));
    });
      _markers.add(
      new Marker(
        width: 45.0,
        height: 45.0,
        point: new latLng.LatLng(point.latitude, point.longitude),
        builder: (cointainer) => new Container(
          child: IconButton(
            icon: Icon(Icons.location_on),
            color: Colors.blue,
            iconSize: 45.0,
            onPressed: () {
              print('marker pressionado');
            },
          ),
        ),
      ),
    ); */
}

class User {
  final int index;
  final String about;
  final String name;
  final String email;
  final String picture;

  User(this.index, this.about, this.name, this.email, this.picture);
}
